if (typeof usabiliticsSettings === 'undefined') {
    var usabiliticsSettings = null;
}

(function (testBotSettings) {
    if (navigator.userAgent === 'TestBot') return;

    var settingsNode = document.querySelector('#usabiliticsSettingsNode');
    if (settingsNode) {
        var settings = JSON.parse(settingsNode.textContent);
        if (settings.processed) {
            // Return when the script is loaded more than once.
            return;
        } else {
            settings.processed = true;
            settingsNode.textContent = JSON.stringify(settings);
        }
    }

    testBotSettings = testBotSettings || JSON.parse(settingsNode.textContent);

    if (!testBotSettings) {
        throw 'No Usabilitics tracking settings are provided.';
    }

    var serverUrl = '@@webServerUrl';
    var cookieName = 'testBotId';

    function getUuid () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
            .replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var cookiesManager = {
        set: function (name, value, expiringDays, domain) {
            var d = new Date();
            d.setTime(d.getTime() + (expiringDays*24*60*60*1000));
            var expires = "expires=" + d.toGMTString();
            var cookie = name + "=" + value + ";path=/; " + expires;
            if(domain) {
                cookie += ";domain=" + domain;
            }
            document.cookie = cookie;
        },

        read: function(name) {
            name += "=";
            var parts = window.document.cookie.split(';');
            for(var i=0; i < parts.length; i++) {
                var part = parts[i].trim();
                if (part.indexOf(name) === 0)
                    return part.substring(name.length, part.length);
            }
            return null;
        }
    };

    function HttpBackend (serverUrl) {
        this.sendData = function (data, successCb) {
            var endpoint = serverUrl + '/v1/collect';

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", endpoint, true);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xmlhttp.send(JSON.stringify(data));
            xmlhttp.onreadystatechange = function() {
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    successCb();
                }
            };
        };
    }

    function WebDriverBackend () {
        this.collectedData = [];
        this.sendData = function (data, successCb) {
            this.collectedData.push(JSON.parse(JSON.stringify(data)));
            successCb();
        };
    }

    var userId;
    if (cookiesManager.read(cookieName)) {
        userId = cookiesManager.read(cookieName);
    } else {
        userId = getUuid();
        cookiesManager.set(cookieName, userId, 365);
    }

    if (getParameterByName('usabilitics') || navigator.userAgent === 'Usabilitics') {
        testBotSettings.backend = new WebDriverBackend();
        testBotSettings.trackAttrs = ['outerHTML'];
        testBotSettings.trackMutations = true;
    } else {
        testBotSettings.backend = testBotSettings.backend || new HttpBackend(serverUrl);
        testBotSettings.trackAttrs = testBotSettings.trackAttrs || [];
        testBotSettings.trackMutations = false;
    }

    var internalApi = (function (settings, userId, appId) {
        var eventsBuffer = [];
        var mutationsBuffer = [];

        function getSelectorForElement (node) {
            var path;
            while (node) {
                var name = node.localName;

                // if (node instanceof HTMLDocument || Object.prototype.toString.call(node) == "[object HTMLDocument]")
                // {
                //     name = 'document';
                // }

                if (!name || name === 'html') break;

                var parent = node.parentNode;

                name = getChildSelector(parent, node);

                path = name + (path ? '>' + path : '');
                node = parent;
            }

            if (!path) path = 'body';

            return path;
        }

        function getChildSelector (parentNode, childNode) {
            if(!childNode || !childNode.localName) return;

            var name = childNode.localName.toLowerCase();

            var siblings = Array.prototype.filter.call(parentNode.childNodes, function (node) {
                return node.nodeType === 1;
            });

            var sameTagSiblings = Array.prototype.filter.call(parentNode.childNodes, function (node) {
                return node.nodeType === 1 && node.localName === name;
            });

            if (sameTagSiblings.length > 1) {
                var index = Array.prototype.indexOf.call(siblings, childNode) + 1;
                if (index > 1) {
                    name = ':nth-child(' + index + ')';
                }
            }

            var idSelector = childNode.getAttribute('id');
            var classes = childNode.getAttribute('class');
            var classSelector = classes ? classes.replace(/\s/g, '.') : '';

            if (idSelector) {
                name += '#' + idSelector;
            }

            //Not a good idea for now, because classes are very dynamic and dependant on the styles.
            // else if (classSelector) {
            //     name += '.' + classSelector;
            // }

            return name;
        }

        function recordEvent (eventName, target, timestamp, fields) {
            if (eventName === 'Visited') {
                eventsBuffer.push({
                    targetSelector: target,
                    type: eventName,
                    userId: userId,
                    appId: appId,
                    timestamp: timestamp,
                    pageUrl: window.location.href
                });
            }
            else {
                var selector = getSelectorForElement(target);
                var entry = {
                    targetSelector: selector,
                    type: eventName,
                    userId: userId,
                    appId: appId,
                    timestamp: timestamp,
                    fields: fields,
                    pageUrl: window.location.href
                };

                for (var i = 0; i < settings.trackAttrs.length; i++) {
                    var attr = settings.trackAttrs[i];
                    entry[attr] = target[attr];
                }

                if (fields) {
                    for (var j = 0; j < fields.length; j++) {
                        var field = fields[j];
                        if (typeof field === 'string' || field instanceof String) {
                            entry[field] = target[field];
                        } else {
                            for (var key in field) {
                                if (field.hasOwnProperty(key)) {
                                    var fieldName = field[key];
                                    entry[key] = target[fieldName];
                                }
                            }
                        }
                    }
                }

                eventsBuffer.push(entry);
            }
        }

        function recordChildListMutationNodes (target, nodes, type, timestamp) {
            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];
                //process only elements for now
                if (node.nodeType !== 1 ||
                    node.localName === 'script' ||
                    node.localName === 'style') continue;

                var targetSelector = getSelectorForElement(target);
                var childSelector = getChildSelector(target, node);

                var mutationSelector = targetSelector ?
                    targetSelector + '>' + childSelector :
                    childSelector;

                var entry = {
                    type: type,
                    mutatedElementSelector: mutationSelector,
                    targetSelector: targetSelector,
                    userId: userId,
                    appId: appId,
                    timestamp: timestamp,
                    pageUrl: window.location.href
                };

                for (var j = 0; j < settings.trackAttrs.length; j++) {
                    var attr = settings.trackAttrs[j];
                    entry[attr] = node[attr];
                }

                mutationsBuffer.push(entry);
            }
        }

        function recordChildListMutation (mutation, timestamp) {
            recordChildListMutationNodes(mutation.target, mutation.addedNodes, 'Added', timestamp);
            recordChildListMutationNodes(mutation.target, mutation.removedNodes, 'Removed', timestamp);
        }

        function sendData () {
            if (eventsBuffer.length === 0 && mutationsBuffer.length === 0)
                return;

            settings.backend.sendData({
                events: eventsBuffer,
                mutations: mutationsBuffer,
                timestamp: Date.now()
            }, function () {
            });

            eventsBuffer.length = 0;
            mutationsBuffer.length = 0;
        }

        // Internal api interface.
        return {
            recordEvent: recordEvent,
            mutationTypes: {
                childList: recordChildListMutation
            },
            sendData: sendData
        };
    })(testBotSettings, userId, testBotSettings.appId);

    // Record page visit when this script is loaded.
    internalApi.recordEvent('Visited', window.location.href, testBotSettings.visitedTimestamp);
    internalApi.sendData();

    /* ------ Attach to events ------ */
    document.addEventListener('click', function (e) {
        internalApi.recordEvent('Click', e.target, Date.now(), [{'targetLocalName': 'localName'}]);
        internalApi.sendData();
    }, true);

    document.addEventListener('focus', function (e) {
        var currentFocus = e.target;
        if (currentFocus.localName === 'input' ||
            currentFocus.localName === 'select' ||
            currentFocus.localName === 'textarea') {
            internalApi.recordEvent('Focus', e.target, Date.now(), ['outerHTML', {inputType: 'type'}, {'targetLocalName': 'localName'}]);
        }
        internalApi.sendData();
    }, true);

    document.addEventListener('keypress', function (e) {
        var currentFocus = e.target;
        if (e.keyCode === 13) { //enter
            var fields = [{'targetLocalName': 'localName'}];
            if (e.target.localName === 'input') {
                fields.push('outerHTML');
                fields.push({inputType: 'type'});
            }
            internalApi.recordEvent('EnterPressed', e.target, Date.now(), fields);
            internalApi.sendData();
        } else if(e.keyCode === 27) { //escape
            internalApi.recordEvent('EscPressed', e.target, Date.now(), [{'targetLocalName': 'localName'}]);
            internalApi.sendData();
        }
    }, true);

    /* ------ Observe mutations ------ */

    if (testBotSettings.trackMutations) {
        var observer = new MutationObserver(function (mutations) {
            var timestamp = Date.now();

            for(var i = 0; i < mutations.length; i++) {
                var mutation = mutations[i];
                internalApi.mutationTypes[mutation.type](mutation, timestamp);
            }

            internalApi.sendData();
        });

        observer.observe(document, {
            subtree: true,
            childList: true,
            attributes: false,
            attributeOldValue: false
        });
    }
})(usabiliticsSettings);