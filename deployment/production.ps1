Set-DefaultAWSRegion -Region us-west-1
Initialize-AWSDefaults
Write-S3Object -BucketName test-bot-tracker -File ..\build\test-bot-tracker.min.js -PublicReadOnly
New-CFInvalidation -DistributionId "E3GQDVRF6K078H" -Paths_Item /test-bot-tracker.min.js -InvalidationBatch_CallerReference "d1" -Paths_Quantity 1
Read-Host -Prompt "Press Enter to exit"