Set-DefaultAWSRegion -Region us-west-1
Initialize-AWSDefaults
Write-S3Object -BucketName test-bot-tracker -File ..\build\test-bot-tracker.local.js -PublicReadOnly
New-CFInvalidation -DistributionId "E3GQDVRF6K078H" -Paths_Item /test-bot-tracker.local.js -InvalidationBatch_CallerReference "d7" -Paths_Quantity 1
Read-Host -Prompt "Press Enter to exit"