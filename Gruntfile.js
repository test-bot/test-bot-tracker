module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
  
      jshint: {
          // define the files to lint
          files: ['gruntfile.js',
              'src/*.js'
          ],
          // configure JSHint (documented at http://www.jshint.com/docs/)
          options: {
              // more options here if you want to override JSHint defaults
              globals: {},
              laxcomma: true
          }
      },
  
      uglify: {
          options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
          },
          production: {
              src: 'src/<%= pkg.name %>.js',
              dest: 'build/<%= pkg.name %>.min.js',
              options: {
                  mangle: true,
                  compress: false,
                  beautify: false,
                  preserveComments: false
              }
          },
          dev: {
              src: 'src/<%= pkg.name %>.js',
              dest: 'build/<%= pkg.name %>.js',
              options: {
                  mangle: false,
                  compress: false,
                  beautify: true,
                  preserveComments: 'all'
              }
          },
          local: {
              src: 'src/<%= pkg.name %>.js',
              dest: 'build/<%= pkg.name %>.local.js',
              options: {
                  mangle: false,
                  compress: false,
                  beautify: true,
                  preserveComments: 'all'
              }
          }
      },
  
      config: {
          remote: {
            options: {
              variables: {
                'webServerUrl': 'http://testbot-web.elasticbeanstalk.com'
              }
            }
          },
          local: {
            options: {
              variables: {
                'webServerUrl': 'https://localhost:8443'
              }
            }
          }
      },

      replace: {
          production: {
            options: {
              variables: {
                'webServerUrl': '<%= grunt.config.get("webServerUrl") %>'
              },
              force: true
            },
            files: [
              {expand: true, flatten: true, src: ['build/<%= pkg.name %>.min.js'], dest: 'build/'}
            ]
          },
          dev: {
            options: {
              variables: {
                'webServerUrl': '<%= grunt.config.get("webServerUrl") %>'
              },
              force: true
            },
            files: [
              {expand: true, flatten: true, src: ['build/<%= pkg.name %>.js'], dest: 'build/'}
            ]
          },
          local: {
            options: {
              variables: {
                'webServerUrl': '<%= grunt.config.get("webServerUrl") %>'
              },
              force: true
            },
            files: [
              {expand: true, flatten: true, src: ['build/<%= pkg.name %>.local.js'], dest: 'build/'}
            ]
          }
        }
    });
  
    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-config');
    grunt.loadNpmTasks('grunt-replace');
  
    // Default task(s).
    grunt.registerTask('default', ['jshint', 'uglify:production', 'config:remote', 'replace:production']);
  
    // Development task(s).
    grunt.registerTask('dev', ['jshint', 'uglify:dev', 'config:remote', 'replace:dev']);
  
    // Builds the script with local web server url.
    grunt.registerTask('local', ['jshint', 'uglify:local', 'config:local', 'replace:local']);
};